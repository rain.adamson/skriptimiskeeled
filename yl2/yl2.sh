#!/bin/bash
# Description :  
# Script that generates new Apache virtualhost , adds hosts entry for testing the site on local computer and
# creates default index.html page fot that virtualhost 
#
# Arguments: 
# [VHOST]   - Vhost you want to create. ie: test.lan 

APACHE_CONF_DEFAULT="/etc/apache2/sites-available/000-default.conf" # Apache virtualhost template file.
TEMPLATE_SERVERNAME="#ServerName www.example.com" 
APACHE_SITES_DIR="/etc/apache2/sites-available/"   # Director where apache virtualhost are stored. 
DEFAULT_ROOT="/var/www/html" # Apache web root 
HOSTS="/etc/hosts"
HOST_IP="127.0.0.1" # IP addess used in hosts entry

if [ $(id -u) -ne 0 ]
then
	echo -e "\e[31m Please run as root :  $0 [ARGUMENTS]  \e[0m" 
	exit 1
fi

# check if Apache2 is available
apache2 -v > /dev/null
if [ $? -eq 0 ]
then
	echo -e "\e[33m Apache2 is already installed \e[0m"
else
	echo -e "\e[33m Apache2 missing, trying to install it! \e[0m"
	echo $(apt-get update && apt-get -y install apache2)
	if [ $= -eq 0 ]
	then
		echo -e "\e[32m Sript installed Apache2! \e[0m"
	else
		echo -e "\e[31m Installing Apache2 failed, exiting! \e[0m"
		exit 1
	fi
fi

# if args == 1 
if [ $# -eq 1 ]
then	
	VHOST=$1
        VHOST_FILE=$VHOST".conf"
	# Check if vhost already exists	
	if [ -e $APACHE_SITES_DIR/$VHOST_FILE ]
	then
		echo "Vhost $VHOST already exists"
		exit 1	
	fi
else
	echo -e "\e[31m please run with following arguments :  VHOST_NAME \e[0m"
	exit 1
fi

function generate_index
{
cat << _EOF_ 
    <!doctype html>
    <html lang="et">
    <head>
    <meta charset="utf-8" />
    <title> $VHOST </title>
    </head>
    <body>
	Teretulemast saidile <strong> $VHOST </strong>
    </body>
    </html>
_EOF_
}

#generate new apache2 conf file from default conf file. 
function conf_from_template
{
	if [ -w $APACHE_SITES_DIR/$VHOST_FILE ]
	then
	        echo -e "\e[33m Virtual host $VHOST_FILE  already exists.  \e[0m" 
		exit 1
	else
		echo -e "\e[32m Generate new conf for site:  $VHOST \e[0m"
		cp $APACHE_CONF_DEFAULT $APACHE_SITES_DIR/$VHOST_FILE 
		if [ $? -eq 0 ]
		then
			
			NEW_ROOT=$DEFAULT_ROOT"/"$VHOST
			CONF_F="$APACHE_SITES_DIR/$VHOST_FILE"
			sed -i "s=$DEFAULT_ROOT=$NEW_ROOT=" "$CONF_F"
			sed -i "s=$TEMPLATE_SERVERNAME=ServerName $VHOST=" "$CONF_F"
		else
			echo "error with cp"
			exit 1 
		fi
	fi
}	


# Generate new entry to hosts files for local teting. 
function add_to_hosts
{

	# Check if host entry already exists
	cat hosts | grep $VHOST >> /dev/null
	if [ $? == 0 ]
	then
        	echo -e "\e[33m host entry for $VHOST already exists  \e[0m" 
        
	else
		echo -e "\e[32m host does not exist! Creating it \e[0m"	
	fi	

	if [ -w $HOSTS ]
	then
        	echo -e "\e[32m adding new entry to hosts file \e[0m" 
		echo -e "$HOST_IP\t$VHOST" >> $HOSTS
        
	else
		echo -e "\e[31m Cant write to hosts file \e[0m"	
		exit 1
	fi	
	
}
# Create index page for Vhost
function create_website
{

	NEW_ROOT=$DEFAULT_ROOT"/"$VHOST
	mkdir $NEW_ROOT	
	generate_index >> $NEW_ROOT/"index.html"
}

# Enable Vhost and reload apache 
function enable_site
{
	# Enable apace vhost site ( symlink sites-availab >  sites-enabled  ) 
	a2ensite $VHOST_FILE > /dev/null
	if [ $? == 0 ]
	then
        	echo -e "\e[32m VHOST $VHOST enabled  \e[0m" 
        
	else
		echo -e "\e[31m Error enableing VHOST $VHOST \e[0m"	
		exit 1
	fi
	# Reload apache conf
	service apache2 reload >> /dev/null
	if [ $? == 0 ]
	then
        	echo -e "\e[32m Apache reloaded  \e[0m" 
        
	else
		echo -e "\e[31m Error reloading apache \e[0m"	
		exit 1
	fi
	}


#MAIN
add_to_hosts
conf_from_template
create_website
enable_site
