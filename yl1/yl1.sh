#!/bin/bash
#Author: Rain A. 
#Description:
# Create new samba shares easily.
# Usage:    ./yl1.sh DIRECTORY GROUP [SHARENAME]
# DIRECTORY - Directory to share. Will be created! 
# GROUP - Usergroup with permission to access the share. If doesn't exist will be created. 
# SHARENAME -  Share name,  if not filled then DIRECTORY is used
# TODO cat smb.conf |  grep -f [sheerinimi]    return 0/1 ? 
#

SAMBA_CONF="/etc/samba/smb.conf"
SAMBA_CONF_TEMP="smb.conf"
SAMBA_CONF_BK="smb.conf.bk"

if [ $(id -u) -ne 0 ]
then
	echo -e "\e[31m Please run as root :  sudo $0 [ARGUMENTS]  \e[0m" 
	exit 1
fi

# check if smbd is available
smbd --version > /dev/null
if [ $? -eq 0 ]
then
	echo -e "\e[33m Samba is already installed \e[0m"
else
	echo -e "\e[33m Samba missing, trying to install it! \e[0m"
	echo $(apt-get update && apt-get -y install samba)
	if [ $= -eq 0 ]
	then
		echo -e "\e[32m Sript installed samba! \e[0m"
	else
		echo -e "\e[31m Installing samba failed, exiting! \e[0m"
		exit 1
	fi
fi


# if args == 2 
if [ $# -eq 2 ]
then	
	KAUST=$1
	GRUPP=$2
	if [[ "$KAUST" = /* ]] # if no SHARE name specified and KAUST happens to be full path then get directory name to use as SHARE name
	then
		SHARE=${KAUST##*/} # get dir name from path
	else
		SHARE=$1
	fi
else
	# if args == 3
	if [ $# -eq 3 ]
	then 
		KAUST=$1
		GRUPP=$2
		SHARE=$3
	else
		# no arguments or more than 3 arguments 
		echo -e "\e[31m please run with following arguments :  KAUST GRUPP [SHARE] \e[0m"
		exit 1
	fi
fi

#Check if directory $KAUST exists 
if [ -d $KAUST ]
then
	echo -e "\e[33m Dir $KAUST already exists. Using it.  \e[0m" 
	
else
	mkdir -p $KAUST && {
	echo -e "\e[32m New folder $KAUST created \e[0m" 
	}
fi


# check if group exists 
getent group $GRUPP > /dev/null
if [ $? -eq 0 ]
then 
	echo -e "\e[33m group $GRUPP already exists. Using it! \e[0m "
else	
	groupadd $GRUPP 
	if [ $? -eq 0 ]
	then 
		echo -e "\e[32m group $GRUPP added \e[0m "
	else
		echo -e "\e[31m adding group $GRUPP failed \e[0m "
	fi 
fi
# If samba config exists 
if [ -w $SAMBA_CONF ] 
then
	# Make a copy of  samba.cnf to current directory
	cp $SAMBA_CONF $PWD
	if [ $? -eq 0 ]
	then 
		echo "samba.conf temporary copy made"
		# also make another copy incase things go terribly wrong
		cp $SAMBA_CONF_TEMP $SAMBA_CONF_BK
	else
		echo -e "\e[31m making a  copy of samba.conf failed \e[0m"
		exit 1  
	fi
else
	echo "Can't make a copy of $SAMBA_CONF , check permissions or if file exists" 
fi


#If samba conf exists and is writable
if [ -w $SAMBA_CONF_TEMP ] 
then
	cat $SAMBA_CONF_TEMP | grep -F "[$SHARE]"
	#echo $? 
	if [ $? -eq 1 ]
	then	
		# if fullpath
		if [ "${KAUST:0:1}" = "/" ]
		then  #full path  
			FPATH=$KAUST
		else # not a  full path, use running directory
			FPATH="$PWD/$KAUST"					
		fi

		echo -e "[$SHARE]\n path=$FPATH \n writeable=yes\n valid users=@$GRUPP\n create mask=0664\n directory mask=0775\n" >> $SAMBA_CONF_TEMP
		#Copy smb.conf back to $SAMBA_CONF directory
                cp $SAMBA_CONF_TEMP $SAMBA_CONF
		#  Check cp smb.conf  exit code 
		if [ $? -eq 0 ]
		then	
			echo "Samba conf copy successful. Will try to restart samba"
			/usr/sbin/service samba reload > /dev/null
			#check if samba reload succeeded.
			if [ $? -eq 0 ]
			then
				echo "Samba reloaded"
			        echo -e "\e[32m All done!  \e[0m" 

				exit 0
			else
				echo -e "\e[31m reloading samba failed. Restoring original smb.conf  \e[0m"
				cp $SAMBA_CONF_BK $SAMBA_CONF

				exit 1
			fi
		else
			echo -e "\e[31mSamba conf copy failed, recheck your config now! \e[0m "
			exit 1
		fi		
	else
	
		echo -e "\e[33m Share already exists \e[0m " 
		exit 1 
	#else
	#	echo -e "\e[31m Error with grep \e[0m"
	#	exit 1
	fi
else
	echo -e "\e[31m Samba conf is not writeable \e[0m "
	exit 1
fi



