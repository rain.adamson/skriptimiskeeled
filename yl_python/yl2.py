#Script that parses input csv file for urls and search strings and searches trough the url for given search strings and output results into file.
#  input file:
#  url1 search1 search2
#  url2 searh1 search2 search3
# Run script with 2 arguments  [input] for input files and [output] for results

import csv
import urllib2
import sys


if len(sys.argv) == 3:
    input= sys.argv[1]
    output= sys.argv[2]

else:
    print "use 2 arguments [input] [output]"


# separator in input file
delimeter=" "

with open(input, 'rb') as f:
    reader = csv.reader(f,delimiter=delimeter, quoting=csv.QUOTE_NONE)
    # read all rows in file
    for row in reader:
        index= 0
        urldata=""
        # parse all items in row
        for item in row:
            if index == 0:
                index=index+1
                url=item
                #print item
                try:
                    # open url
                    f = urllib2.urlopen(item)
                    urlData= f.read()
                except:
                    print("ERROR: invalid url: " + item)
                #print urlData
            # loop searchstrings
            elif index >= 1:
                with open(output, "a") as outfile:
                    #if page source contains string
                    if urlData.find(item) != -1:
                        outfile.write(url + " " + item + " JAH\n")
                    else:
                        outfile.write(url + " " + item + " EI\n")


